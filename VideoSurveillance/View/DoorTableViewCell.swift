//
//  DoorTableViewCell.swift
//  VideoSurveillance
//
//  Created by Татьяна Аникина on 13.08.2023.
//

import UIKit

class DoorTableViewCell: UITableViewCell {
    
    @IBOutlet weak var doorName: UILabel!
    @IBOutlet weak var doorImage: UIImageView!
    @IBOutlet weak var containerView: UIView! {
        didSet {
            containerView.layer.cornerRadius = 10
        }
    }
    
    func getImage(url: URL) {
        guard doorImage.image == nil else {return}
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            guard let imageData = data else { return }
            
            DispatchQueue.main.async { [unowned self] in
                self.doorImage.isHidden = false
                self.doorImage.image = UIImage(data: imageData)
            }
        }.resume()
    }
  
    override func prepareForReuse() {
        doorImage.image = nil
        doorImage.isHidden = true
    }
}
