//
//  CameraTableViewCell.swift
//  VideoSurveillance
//
//  Created by Татьяна Аникина on 13.08.2023.
//

import UIKit

class CameraTableViewCell: UITableViewCell {

    @IBOutlet weak var cameraImage: UIImageView!{
        didSet {
            cameraImage.layer.cornerRadius = 10
            cameraImage.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        }
    }
    @IBOutlet weak var guardoffImage: UIImageView!
    @IBOutlet weak var cameraName: UILabel!
    @IBOutlet weak var favImage: UIImageView!
    @IBOutlet weak var containerView: UIView! {
        didSet {
            containerView.layer.cornerRadius = 10
        }
    }
    
    func getImage(url: URL) {
        guard cameraImage.image == nil else {return}
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            guard let imageData = data else { return }
            
            DispatchQueue.main.async { [unowned self] in
                self.cameraImage.image = UIImage(data: imageData)
            }
        }.resume()
    }
}
