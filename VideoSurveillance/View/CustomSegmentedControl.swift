//
//  CustomSegmentedControl.swift
//  VideoSurveillance
//
//  Created by Татьяна Аникина on 12.08.2023.
//

import UIKit

@IBDesignable class CustomSegmentedControl: UIControl {
    
    private var labels = [UILabel]()
    var selectionView = UIView()
    
    var items: [String] = ["Камеры", "Двери"] {
        didSet {
            setupLabels()
        }
    }
    
    var selectedIndex : Int = 0 {
        didSet {
            displayNewSelectedIndex()
        }
    }
    
    @IBInspectable var selectedLabelColor : UIColor = .black {
        didSet {
            setSelectedColors()
        }
    }
    
    @IBInspectable var unselectedLabelColor : UIColor = .white {
        didSet {
            setSelectedColors()
        }
    }
    
    @IBInspectable var selectionColor : UIColor = UIColor.white {
        didSet {
            setSelectedColors()
        }
    }
    
    @IBInspectable var borderColor : UIColor = .white {
        didSet {
            layer.borderColor = borderColor.cgColor
        }
    }
    
    @IBInspectable var font : UIFont! = UIFont.systemFont(ofSize: 15) {
        didSet {
            setFont()
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupView()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setupView()
    }
    
    func setupView(){
        
        backgroundColor = .clear
        
        setupLabels()
        
        addIndividualItemConstraints(items: labels, mainView: self, padding: 0)
        
        insertSubview(selectionView, at: 0)
    }
    
    func setupLabels(){
        
        for label in labels {
            label.removeFromSuperview()
        }
        
        labels.removeAll(keepingCapacity: true)
        
        for index in 1...items.count {
            
            let label = UILabel(frame: CGRectMake(0, 0, 70, 40))
            label.text = items[index - 1]
            label.backgroundColor = .clear
            label.textAlignment = .center
            label.font = UIFont.systemFont(ofSize: 15)
            label.textColor = index == 1 ? selectedLabelColor : unselectedLabelColor
            label.translatesAutoresizingMaskIntoConstraints = false
            self.addSubview(label)
            labels.append(label)
        }
        
        addIndividualItemConstraints(items: labels, mainView: self, padding: 0)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        var selectFrame = self.bounds
        let newWidth = CGRectGetWidth(selectFrame) / CGFloat(items.count)
        selectFrame.size.width = newWidth
        selectionView.frame = CGRect(x: selectFrame.origin.x,
                                 y: selectFrame.origin.y + selectFrame.height,
                                 width: selectFrame.size.width,
                                     height: 2)
        selectionView.backgroundColor = selectionColor
        
        displayNewSelectedIndex()
        
    }
    
    override func beginTracking(_ touch: UITouch, with event: UIEvent?) -> Bool {
        
        let location = touch.location(in: self)
        
        var calculatedIndex : Int?
        for (index, item) in labels.enumerated() {
            if item.frame.contains(location) {
                calculatedIndex = index
            }
        }
        
        
        if calculatedIndex != nil {
            selectedIndex = calculatedIndex!
            sendActions(for: .valueChanged)
        }
        
        return false
    }
    
    func displayNewSelectedIndex(){
        for item in labels {
            item.textColor = unselectedLabelColor
        }
        
        let label = labels[selectedIndex]
        label.textColor = selectedLabelColor
      
        UIView.animate(withDuration: 0.5, delay: 0.0, usingSpringWithDamping: 0.5, initialSpringVelocity: 0.8, animations: {
            
            self.selectionView.center = CGPoint(x: label.frame.origin.x + label.frame.width / 2,
                                                y: label.frame.origin.y + label.frame.height)
            
            }, completion: nil)
    }
    
    func addIndividualItemConstraints(items: [UIView], mainView: UIView, padding: CGFloat) {
        
        let constraints = mainView.constraints
        
        for (index, button) in items.enumerated() {
            
            let topConstraint = button.topAnchor.constraint(equalTo: mainView.topAnchor, constant: 0)
            
            let bottomConstraint = button.bottomAnchor.constraint(equalTo: mainView.bottomAnchor, constant: 0)
            
            var rightConstraint : NSLayoutConstraint!
            
            if index == items.count - 1 {
                
                rightConstraint = button.trailingAnchor.constraint(equalTo: mainView.trailingAnchor, constant: -padding)
                
            } else {
                
                let nextButton = items[index+1]
                rightConstraint = button.trailingAnchor.constraint(equalTo: nextButton.leadingAnchor, constant: -padding)
            }
            
            
            var leftConstraint : NSLayoutConstraint!
            
            if index == 0 {
                
                leftConstraint = button.leadingAnchor.constraint(equalTo: mainView.leadingAnchor, constant: padding)
                
            } else {
                
                let prevButton = items[index-1]
                leftConstraint = button.leadingAnchor.constraint(equalTo: prevButton.trailingAnchor, constant: padding)
                
                let firstItem = items[0]
                
                var widthConstraint = button.widthAnchor.constraint(equalTo: firstItem.widthAnchor)
                
                mainView.addConstraint(widthConstraint)
            }
            
            mainView.addConstraints([topConstraint, bottomConstraint, rightConstraint, leftConstraint])
        }
    }
    
    func setSelectedColors(){
        for item in labels {
            item.textColor = unselectedLabelColor
        }
        
        if labels.count > 0 {
            labels[0].textColor = selectedLabelColor
        }
        
        selectionView.backgroundColor = selectionColor
    }
    
    func setFont(){
        for item in labels {
            item.font = font
        }
    }
}
