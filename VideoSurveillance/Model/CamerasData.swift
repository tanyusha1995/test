//
//  CamerasData.swift
//  VideoSurveillance
//
//  Created by Татьяна Аникина on 10.08.2023.
//

import Foundation

struct CamerasData: Codable {
    let room: [String]
    let cameras: [CameraData]
}
