//
//  CameraData.swift
//  VideoSurveillance
//
//  Created by Татьяна Аникина on 10.08.2023.
//

import Foundation
import RealmSwift

struct CameraData: Codable {
    let name: String
    let snapshot: String?
    let room: String?
    let id: Int
    let favorites: Bool
    let rec: Bool
}

class CameraDataObject: Object {
    @Persisted(primaryKey: true) var id: Int
    @Persisted var name: String = ""
    @Persisted var snapshot: String
    @Persisted var favorites: Bool
    convenience init(id: Int, name: String, favorites: Bool, snapshot: String) {
        self.init()
        self.id = id
        self.name = name
        self.favorites = favorites
        self.snapshot = snapshot
    }
    
    static func createFrom(cameraData: [CameraData]) -> [CameraDataObject] {
        cameraData.map { cameraInfo in
            CameraDataObject(id: cameraInfo.id,
                             name: cameraInfo.name,
                             favorites: cameraInfo.favorites,
                             snapshot: cameraInfo.snapshot ?? "" )
        }
    }
}
