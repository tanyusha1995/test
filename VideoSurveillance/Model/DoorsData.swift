//
//  DoorsData.swift
//  VideoSurveillance
//
//  Created by Татьяна Аникина on 10.08.2023.
//

import Foundation

struct DoorsData: Codable {
    let success: Bool
    let data: [DoorData]
}
