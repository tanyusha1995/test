//
//  DoorData.swift
//  VideoSurveillance
//
//  Created by Татьяна Аникина on 10.08.2023.
//

import Foundation
import RealmSwift

struct DoorData: Codable {
    let name: String
    let room: String?
    let id: Int
    let favorites: Bool
    let snapshot: String?
}

class DoorDataObject: Object {
    @Persisted(primaryKey: true) var id: Int
    @Persisted var name: String = ""
    @Persisted var snapshot: String
    @Persisted var favorites: Bool
    convenience init(id: Int, name: String, favorites: Bool, snapshot: String) {
        self.init()
        self.id = id
        self.name = name
        self.favorites = favorites
        self.snapshot = snapshot
    }
    
    static func createFrom(doorData: [DoorData]) -> [DoorDataObject] {
        doorData.map { doorInfo in
            DoorDataObject(id: doorInfo.id,
                           name: doorInfo.name,
                           favorites: doorInfo.favorites,
                           snapshot: doorInfo.snapshot ?? "" )
        }
    }
}
