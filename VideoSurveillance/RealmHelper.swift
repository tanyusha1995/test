//
//  RealmHelper.swift
//  VideoSurveillance
//
//  Created by Татьяна Аникина on 12.08.2023.
//

import Foundation
import RealmSwift

class RealmHelper {
    
    static let shared = RealmHelper()
    private init(){}
    
    var realm: Realm {
        try! Realm()
    }
    
    func write(objects: [Object]) {
        do {
            try realm.write {
                realm.add(objects,update: .all)
            }
        } catch {
            print(error)
        }
    }
    
    func allDoors() -> Results<DoorDataObject> {
        realm.objects(DoorDataObject.self)
    }
    
    func allCameras() -> Results<CameraDataObject> {
        realm.objects(CameraDataObject.self)
    }
    
}
