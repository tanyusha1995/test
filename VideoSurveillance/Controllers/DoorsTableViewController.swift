//
//  DoorsTableViewController.swift
//  VideoSurveillance
//
//  Created by Татьяна Аникина on 13.08.2023.
//

import UIKit
import RealmSwift

private let reuseIdentifier = "DoorsTableViewCell"

class DoorsTableViewController: UITableViewController {

    weak var actionDelegate: MainViewController?
    
    private var doRefresh = false
    private var doorData: Results<DoorDataObject>? {
        RealmHelper.shared.allDoors()
    }
    override func viewDidLoad() {
        super.viewDidLoad()

        addRefresher()
        loadDoorsData()
    }
    
    private func addRefresher() {
        let refresher = UIRefreshControl()
        tableView.alwaysBounceVertical = true
        refresher.tintColor = UIColor.gray
        refresher.addTarget(self,
                            action: #selector(refresh),
                            for: . valueChanged)
        tableView.refreshControl = refresher
    }
    
    @objc private func refresh(){
        doRefresh = true
        loadDoorsData()
    }
    
    private func loadDoorsData() {
        let doorsUrl = "http://cars.cprogroup.ru/api/rubetek/doors/"
        guard let url = URL(string: doorsUrl) else { return }
        if doRefresh || doorData?.isEmpty ?? true {
            doRefresh = false
            URLSession.shared.dataTask(with: url) { [unowned self] data, responce, error in
                guard let data = data else { return }
                guard error == nil else { return }
                do {
                    let doorsData = try JSONDecoder().decode(DoorsData.self, from: data)
                    RealmHelper.shared.write(objects: DoorDataObject.createFrom(doorData: doorsData.data))
                    DispatchQueue.main.async {
                        self.tableView.refreshControl?.endRefreshing()
                        self.tableView.reloadData()
                    }
                } catch let error {
                    print(error)
                }
            }.resume()
        }
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return doorData?.count ?? 0
    }
   
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "DoorTableViewCell", for: indexPath) as! DoorTableViewCell
        let doorInfo = doorData?[indexPath.row]
        if let url = URL(string: doorInfo?.snapshot ?? "") {
            cell.getImage(url: url)
        }
        cell.doorName.text = doorInfo?.name

        return cell
    }

    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let doorInfo = doorData?[indexPath.row]
        actionDelegate?.showOpenDoorController(doorInfo: doorInfo!)
    }
    
    override func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
    
        let favoriteAction = UIContextualAction(style: .normal, title: nil) { (_, _, completionHandler) in
              }
        
        favoriteAction.backgroundColor = UIColor(red: 246/255, green: 246/255, blue: 246/255, alpha: 1.0)
        favoriteAction.image = UIImage(named: "starRound")
        
        let editAction = UIContextualAction(style: .normal, title: nil) { (_, _, completionHandler) in
              }

        editAction.backgroundColor = UIColor(red: 246/255, green: 246/255, blue: 246/255, alpha: 1.0)
        editAction.image = UIImage(named: "edit")
              let configurationTwo = UISwipeActionsConfiguration(actions: [ favoriteAction, editAction])
        
            return configurationTwo
      }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let doorInfo = doorData?[indexPath.row]
        if let url = URL(string: doorInfo?.snapshot ?? "") {
            return 310
        } else {
            return  80
        }
    }
}
