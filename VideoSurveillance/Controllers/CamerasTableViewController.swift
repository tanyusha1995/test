//
//  CamerasTableViewController.swift
//  VideoSurveillance
//
//  Created by Татьяна Аникина on 13.08.2023.
//

import UIKit
import RealmSwift

private let reuseIdentifier = "CameraTableViewCell"

class CamerasTableViewController: UITableViewController {
    
    private var doRefresh = false
    private var cameraData: Results<CameraDataObject>? {
        RealmHelper.shared.allCameras()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        loadCamerasData()
        addRefresher()
    }

    private func addRefresher() {
        let refresher = UIRefreshControl()
        tableView.alwaysBounceVertical = true
        refresher.tintColor = UIColor.gray
        refresher.addTarget(self,
                            action: #selector(refresh),
                            for: . valueChanged)
        tableView!.refreshControl = refresher
    }
    
    @objc private func refresh(){
        doRefresh = true
        loadCamerasData()
    }
    
    private func loadCamerasData() {
        let camerasUrl = "http://cars.cprogroup.ru/api/rubetek/cameras/"
        guard let url = URL(string: camerasUrl) else { return }
        if doRefresh || cameraData?.isEmpty ?? true {
            doRefresh = false
            URLSession.shared.dataTask(with: url) { [unowned self] data, responce, error in
                guard let data = data else { return }
                guard error == nil else { return }
                do {
                    let camerasData = try JSONDecoder().decode(NameData.self, from: data)
                    RealmHelper.shared.write(objects: CameraDataObject.createFrom(cameraData: camerasData.data.cameras))
                       DispatchQueue.main.async {
                           self.tableView.refreshControl?.endRefreshing()
                           self.tableView.reloadData()
                       }
                   } catch let error {
                       print(error)
                   }
               }.resume()
           }
       }
    
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cameraData?.count ?? 0
    }
    
    override func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
    
        let favoriteAction = UIContextualAction(style: .normal, title: nil) { (_, _, completionHandler) in
              }
        
        favoriteAction.backgroundColor = UIColor(red: 246/255, green: 246/255, blue: 246/255, alpha: 1.0)
        favoriteAction.image = UIImage(named: "starRound")
              let configurationOne = UISwipeActionsConfiguration(actions: [favoriteAction])
        
            return configurationOne
      }

    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "Гостинная"
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CameraTableViewCell", for: indexPath) as! CameraTableViewCell
        let cameraInfo = cameraData?[indexPath.row]
        if let url = URL(string: cameraInfo?.snapshot ?? "") {
            cell.getImage(url: url)
        }
    
        cell.cameraName.text = cameraInfo?.name
        if cameraInfo?.favorites == true{
            cell.favImage.image = UIImage(systemName: "star.fill")
            cell.favImage.tintColor = .systemYellow
        } else if cameraInfo?.favorites == false {
            cell.favImage.image = UIImage(named: "")
        }

        return cell
    }
}
