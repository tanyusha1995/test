//
//  MainViewController.swift
//  VideoSurveillance
//
//  Created by Татьяна Аникина on 09.08.2023.
//

import UIKit

class MainViewController: UIViewController {
    
    lazy var camerasTVC: CamerasTableViewController = {
        return UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CamerasTableViewController") as! CamerasTableViewController
    }()
    
    lazy var doorsTVC: DoorsTableViewController = {
        let doorsController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "DoorsTableViewController") as! DoorsTableViewController
        doorsController.actionDelegate = self
        return doorsController
    }()
    
    @IBOutlet weak var segmentControl: CustomSegmentedControl!
    @IBOutlet weak var containerView: UIView!
    
    @IBAction func segmentControlDidChangeValue(_ sender: CustomSegmentedControl) {
        
        if sender.selectedIndex == 0 {
            switchViewController(from: doorsTVC, to: camerasTVC)
        } else {
            switchViewController(from: camerasTVC, to: doorsTVC)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "Мой дом"
        addCamerasController()
    }
    
    private func addCamerasController() {
        let camerasView = camerasTVC.tableView!
        addChild(camerasTVC)
        addSubview(subview: camerasView)
        camerasTVC.didMove(toParent: self)
        
    }
    
    private func addSubview(subview: UIView) {
        subview.translatesAutoresizingMaskIntoConstraints = false
        containerView.addSubview(subview)
        
        NSLayoutConstraint.activate([
            subview.topAnchor.constraint(equalTo: containerView.topAnchor),
            subview.bottomAnchor.constraint(equalTo: containerView.bottomAnchor),
            subview.leadingAnchor.constraint(equalTo: containerView.leadingAnchor),
            subview.trailingAnchor.constraint(equalTo: containerView.trailingAnchor)
        ])
    }
    
    private func switchViewController(from fromVC: UIViewController?, to toVC: UIViewController?) {
        if fromVC != nil {
            fromVC!.willMove(toParent: nil)
            fromVC!.view.removeFromSuperview()
            fromVC!.removeFromParent()
        }
        if toVC != nil {
            addChild(toVC!)
            addSubview(subview: toVC!.view)
            toVC!.didMove(toParent: self)
        }
    }
    
    func showOpenDoorController(doorInfo: DoorDataObject) {
        let con = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "OpenDoorViewController") as! OpenDoorViewController
        
        navigationController?.pushViewController(con, animated: true)
    }
}
